package AIF.Modules;

public class AttackModule extends Module {
    private static final int RANGE = 1500;

    public AttackModule() {
        super(RANGE);
    }

    @Override
    public void activate() {
        this.updateCanBeActivated(false);
        System.out.println("Activated Attack! pew pew");
    }
}

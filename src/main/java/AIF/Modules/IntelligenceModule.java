package AIF.Modules;

public class IntelligenceModule extends Module {
    private static final int RANGE = 600;

    public IntelligenceModule() {
        super(RANGE);
    }

    @Override
    public void activate() {
        System.out.println("Activated Intelligence! burp burp");
    }
}

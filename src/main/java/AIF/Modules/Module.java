package AIF.Modules;

public abstract class Module {
    private final int range;
    protected boolean canBeActivated;

    public Module(int range) {
        this.canBeActivated = true;
        this.range = range;
    }

    public int range() {
        return this.range;
    }

    public boolean canBeActivated() {
        return this.canBeActivated;
    }

    protected void updateCanBeActivated(boolean canBeActivated) {
        this.canBeActivated = canBeActivated;
    }

    public abstract void activate();
}

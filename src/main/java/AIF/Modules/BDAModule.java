package AIF.Modules;

public class BDAModule extends Module {
    private static final int RANGE = 300;

    public BDAModule() {
        super(RANGE);
    }

    @Override
    public void activate() {
        System.out.println("Activated BDA! calc calc");
    }
}

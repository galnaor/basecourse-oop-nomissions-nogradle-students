package AIF.AerialVehicles.FighterJets;

import AIF.Entities.Coordinates;
import AIF.Modules.AttackModule;
import AIF.Modules.IntelligenceModule;

public class F15 extends FlighterJet {
    private static final int STATIONS_AMOUNT = 10;
    private static final String[] COMPATIBLE_MODULES = {AttackModule.class.getName(), IntelligenceModule.class.getName()};

    public F15(Coordinates startingLocation) {
        super(startingLocation, STATIONS_AMOUNT, COMPATIBLE_MODULES);
    }
}

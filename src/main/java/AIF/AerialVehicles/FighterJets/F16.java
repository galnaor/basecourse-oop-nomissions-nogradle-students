package AIF.AerialVehicles.FighterJets;

import AIF.Entities.Coordinates;
import AIF.Modules.AttackModule;
import AIF.Modules.BDAModule;

public class F16 extends FlighterJet {
    private static final int STATIONS_AMOUNT = 7;
    private static final String[] COMPATIBLE_MODULES = {AttackModule.class.getName(), BDAModule.class.getName()};

    public F16(Coordinates startingLocation) {
        super(startingLocation, STATIONS_AMOUNT, COMPATIBLE_MODULES);
    }
}

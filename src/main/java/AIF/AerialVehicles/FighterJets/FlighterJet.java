package AIF.AerialVehicles.FighterJets;

import AIF.AerialVehicles.AerialVehicle;
import AIF.Entities.Coordinates;

public abstract class FlighterJet extends AerialVehicle {
    private static final int MAINTENANCE_DISTANCE = 25000;

    public FlighterJet(Coordinates startingLocation, int stationsAmount, String[] compatibleModules) {
        super(MAINTENANCE_DISTANCE, startingLocation, stationsAmount, compatibleModules);
    }
}

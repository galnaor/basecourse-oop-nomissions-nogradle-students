package AIF.AerialVehicles.UAVs.Hermes;

import AIF.Entities.Coordinates;
import AIF.Modules.BDAModule;
import AIF.Modules.IntelligenceModule;

public class Zik extends Hermes {
    private static final int STATIONS_AMOUNT = 1;
    private static final String[] COMPATIBLE_MODULES = {BDAModule.class.getName(), IntelligenceModule.class.getName()};

    public Zik(Coordinates startingLocation) {
        super(startingLocation, STATIONS_AMOUNT, COMPATIBLE_MODULES);
    }
}
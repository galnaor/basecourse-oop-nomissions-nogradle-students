package AIF.AerialVehicles.UAVs.Hermes;

import AIF.AerialVehicles.UAVs.UAV;
import AIF.Entities.Coordinates;

public abstract class Hermes extends UAV {
    private static final int MAINTENANCE_DISTANCE = 10000;

    public Hermes(Coordinates startingLocation, int stationsAmount, String[] compatibleModules) {
        super(MAINTENANCE_DISTANCE, startingLocation, stationsAmount, compatibleModules);
    }
}

package AIF.AerialVehicles.UAVs.Hermes;

import AIF.Entities.Coordinates;
import AIF.Modules.AttackModule;
import AIF.Modules.BDAModule;
import AIF.Modules.IntelligenceModule;

public class Kochav extends Hermes {
    private static final int STATIONS_AMOUNT = 5;
    private static final String[] COMPATIBLE_MODULES = {AttackModule.class.getName(), BDAModule.class.getName(), IntelligenceModule.class.getName()};

    public Kochav(Coordinates startingLocation) {
        super(startingLocation, STATIONS_AMOUNT, COMPATIBLE_MODULES);
    }
}

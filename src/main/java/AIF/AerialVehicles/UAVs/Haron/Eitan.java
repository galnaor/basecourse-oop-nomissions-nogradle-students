package AIF.AerialVehicles.UAVs.Haron;

import AIF.Entities.Coordinates;
import AIF.Modules.AttackModule;
import AIF.Modules.IntelligenceModule;

public class Eitan extends Haron {
    private static final int STATIONS_AMOUNT = 4;
    private static final String[] COMPATIBLE_MODULES = {AttackModule.class.getName(), IntelligenceModule.class.getName()};

    public Eitan(Coordinates startingLocation) {
        super(startingLocation, STATIONS_AMOUNT, COMPATIBLE_MODULES);
    }
}

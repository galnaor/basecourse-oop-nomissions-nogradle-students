package AIF.AerialVehicles.UAVs.Haron;

import AIF.AerialVehicles.UAVs.UAV;
import AIF.Entities.Coordinates;

public abstract class Haron extends UAV {
    private static final int MAINTENANCE_DISTANCE = 15000;

    public Haron(Coordinates startingLocation, int stationsAmount, String[] compatibleModules) {
        super(MAINTENANCE_DISTANCE, startingLocation, stationsAmount, compatibleModules);
    }
}

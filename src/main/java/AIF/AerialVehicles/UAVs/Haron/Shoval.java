package AIF.AerialVehicles.UAVs.Haron;

import AIF.Entities.Coordinates;
import AIF.Modules.AttackModule;
import AIF.Modules.BDAModule;
import AIF.Modules.IntelligenceModule;

public class Shoval extends Haron {
    private static final int STATIONS_AMOUNT = 3;
    private static final String[] COMPATIBLE_MODULES = {AttackModule.class.getName(), BDAModule.class.getName(), IntelligenceModule.class.getName()};

    public Shoval(Coordinates startingLocation) {
        super(startingLocation, STATIONS_AMOUNT, COMPATIBLE_MODULES);
    }
}

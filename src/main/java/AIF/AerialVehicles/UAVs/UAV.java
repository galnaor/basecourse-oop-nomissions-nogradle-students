package AIF.AerialVehicles.UAVs;

import AIF.AerialVehicles.AerialVehicle;
import AIF.AerialVehicles.Exceptions.CannotPerformOnGroundException;
import AIF.Entities.Coordinates;

public abstract class UAV extends AerialVehicle {
    private static final int SPEED = 150;
    public UAV(int maintenanceDistance, Coordinates startingLocation, int stationsAmount, String[] compatibleModules) {
        super(maintenanceDistance, startingLocation, stationsAmount, compatibleModules);
    }

    public void hoverOverLocation(double hours) throws CannotPerformOnGroundException {
        if (this.isOnGround) {
            throw new CannotPerformOnGroundException();
        }

        System.out.println("Hovering over:" + this.currentLocation);
        this.updateDistanceFromLastMaintenance(this.distanceFromLastMaintenance + SPEED * hours);
    }
}

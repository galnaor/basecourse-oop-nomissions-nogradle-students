package AIF.AerialVehicles;

import AIF.AerialVehicles.Exceptions.*;
import AIF.Entities.Coordinates;
import AIF.Modules.Module;

import java.util.ArrayList;
import java.util.Arrays;

public abstract class AerialVehicle {
    protected final int MAINTENANCE_DISTANCE;
    protected final int STATIONS_AMOUNT;
    protected final String[] COMPATIBLE_MODULES;
    protected ArrayList<Module> modules;
    protected Coordinates currentLocation;
    protected boolean isOnGround;
    protected double distanceFromLastMaintenance;

    public AerialVehicle(int maintenanceDistance, Coordinates startingLocation, int stationsAmount, String[] compatibleModules) {
        this.isOnGround = true;
        this.distanceFromLastMaintenance = 0;
        this.modules = new ArrayList<>();
        this.COMPATIBLE_MODULES = compatibleModules;
        this.MAINTENANCE_DISTANCE = maintenanceDistance;
        this.currentLocation = startingLocation;
        this.STATIONS_AMOUNT = stationsAmount;
    }

    public void takeOff() throws CannotPerformInMidAirException, NeedMaintenanceException {
        if (!this.needMaintenance() && this.isOnGround) {
            System.out.println("Taking off");
            this.updateIsOnGround(false);
        } else {
            if (!this.isOnGround) {
                throw new CannotPerformInMidAirException();
            }

            throw new NeedMaintenanceException();
        }
    }

    public void flyTo(Coordinates destination) throws CannotPerformOnGroundException {
        if (this.isOnGround) {
            throw new CannotPerformOnGroundException();
        }

        System.out.println("Flying to: " + destination.toString());
        this.updateDistanceFromLastMaintenance(this.distanceFromLastMaintenance + this.currentLocation.distance(destination));
        this.updateCurrentLocation(destination);
    }

    public void land() throws CannotPerformOnGroundException {
        if (this.isOnGround) {
            throw new CannotPerformOnGroundException();
        }

        System.out.println("Landing");
        this.updateIsOnGround(true);
    }

    public boolean needMaintenance() {
        return this.distanceFromLastMaintenance >= this.MAINTENANCE_DISTANCE;
    }

    public void performMaintenance() throws CannotPerformInMidAirException {
        if (!this.isOnGround) {
            throw new CannotPerformInMidAirException();
        }

        System.out.println("Performing maintenance");
        this.updateDistanceFromLastMaintenance(0);
    }

    protected void updateIsOnGround(boolean isOnGround) {
        this.isOnGround = isOnGround;
    }

    protected void updateDistanceFromLastMaintenance(double distanceFromLastMaintenance) {
        this.distanceFromLastMaintenance = distanceFromLastMaintenance;
    }

    protected void updateCurrentLocation(Coordinates location) {
        this.currentLocation = location;
    }

    public void loadModule(Module moduleToLoad) throws NoModuleStationAvailableException, ModuleNotCompatibleException {
        if (this.modules.size() == this.STATIONS_AMOUNT) {
            throw new NoModuleStationAvailableException();
        }

        if (!Arrays.asList(this.COMPATIBLE_MODULES).contains(moduleToLoad.getClass().getName())) {
            throw new ModuleNotCompatibleException();
        }

        this.modules.add(moduleToLoad);
        System.out.println("Module " + moduleToLoad.getClass().getSimpleName() + " loaded");
    }

    public void activateModule(Class<? extends Module> moduleToActivate, Coordinates targetLocation) throws ModuleNotCompatibleException, ModuleNotFoundException, NoModuleCanPerformException {
        if (!Arrays.asList(this.COMPATIBLE_MODULES).contains(moduleToActivate.getName())) {
            throw new ModuleNotCompatibleException();
        }

        Module foundModule = findModule(moduleToActivate.getName(), targetLocation);
        foundModule.activate();
        System.out.println("Module " + moduleToActivate.getSimpleName() + " activated on target " + targetLocation);
    }

    private boolean canModuleBeActivated(Module module, Coordinates targetLocation) {
        return module.canBeActivated() && (currentLocation.distance(targetLocation) <= module.range());
    }

    private Module findModule(String moduleClassName, Coordinates targetLocation) throws ModuleNotFoundException, NoModuleCanPerformException {
        boolean isFound = false;

        for (Module module : this.modules) {
            if (module.getClass().getName().equals(moduleClassName)) {
                isFound = true;
                if (this.canModuleBeActivated(module, targetLocation)) {
                    return module;
                }
            }
        }

        if (isFound) {
            throw new NoModuleCanPerformException();
        }

        throw new ModuleNotFoundException();
    }
}
